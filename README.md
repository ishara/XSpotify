# XSpotify

### A modified Spotify client for Windows
![Screenshot](https://i.imgur.com/iJ0z7vn.png)

[![HitCount](http://hits.dwyl.io/meik97/XSpotify.svg)](https://github.com/meik97/XSpotify)
[![Downloads](https://img.shields.io/github/downloads/meik97/XSpotify/total.svg?color=green)](https://github.com/meik97/XSpotify/releases)
[![Discord](https://discordapp.com/api/guilds/671076782467973130/widget.png)](http://discord.gg/EByRp27)


## Features:

- DRM bypass: Download all songs directly from Spotify servers --> https://www.youtube.com/watch?v=E8a-dB1HIQU
- Enabled the skip button for ads blocking
- Auto skipping function for audio based ads (Disabled in current build)
- Quality and format (free account): 160 kb/s, 32-bit, 44100 Hz .ogg
- Quality and format (premium account): 320 kb/s, 32-bit, 44100 Hz .ogg
- Metadata: artist, title, album and album cover


## Requirements:

You need Microsoft's [Visual C++ Redistributable Runtimes](https://github.com/abbodi1406/vcredist) installed.

## How to use:

- [Download XSpotify](https://github.com/meik97/XSpotify/releases)
- Place all files into your Spotify profile, it's located under `%appdata%\Spotify`.
- Start Spotify

After you have started XSpotify, the console window will appear. 
  
  
![Command Line Interface](https://i.imgur.com/uRwqF2L.png)
  
## Tested versions:

- 1.1.25.559.g85cf5e4c (30.01.2020)

## Known bugs:

- Crash after song skipping

## Thanks to:
- fortender (https://github.com/fortender)
- pr8x (https://github.com/pr8x)

## Tools:
- OpenSSL
- TagLib
