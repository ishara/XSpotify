namespace Modules
{
	class Bitrate
	{
	public:
		Bitrate();

	private:	
		static int SyncBitrateEnumeration_hk(signed int a1);
		static signed int SetBitrate_hk(int a1);
		static signed int SetBitrate2_hk(int a1);

		static void ExecBitrate_stub(DWORD* a1, void* a2);
		static void CheckBitrate_stub(signed int* a1, int a2, int a3, int a4, int a5);
		static void BitrateSetting_stub(int a1, int a2, int a3, int* a4);
		static void SelectOGGFormat_stub(int a1, int a2, int a3);
	};
}