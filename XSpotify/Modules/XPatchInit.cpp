#pragma once
#include "..\\include\BaseInclude.hpp"

namespace Modules
{
	void Initialization::XSpotifyWelcome()
	{
		HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
		SetConsoleTextAttribute(hConsole, 10);
		std::cout << R"(
	                            __   __ _____             _   _  __       
	                            \ \ / // ____|           | | (_)/ _|      
	                             \ V /| (___  _ __   ___ | |_ _| |_ _   _ 
	                              > <  \___ \| '_ \ / _ \| __| |  _| | | |
	                             / . \ ____) | |_) | (_) | |_| | | | |_| |
	                            /_/ \_\_____/| .__/ \___/ \__|_|_|  \__, |
	                                         | |                     __/ |
	                                         |_|                    |___/  )" << std::endl;
	}

	void Initialization::CheckFiles()
	{
		if (!Utils::Utils::FileExists("token.exe"))
		{
			std::cout << "Token.exe not found..." << std::endl;
		}

		if (!Utils::Utils::PathExists(".\\XSongs"))
		{
			std::cout << "XSongs folder not found..." << std::endl;
		}

		//incase someone didnt copy the .exe or is trying to use the store version
		if (Utils::Utils::GetFileSize("Spotify.exe") != 22240160)
		{
			std::cout << "Wrong Spotify version..." << std::endl;
		}
	}

	//void SkipSong_hotfix()
	//{
	//
	//}
	//
	//void ReadConfig()
	//{
	//	
	//}
	//
	//void SetDownloadLocation()
	//{
	//	
	//}

	Initialization::Initialization()
	{
		Initialization::XSpotifyWelcome();
		Initialization::CheckFiles();
	}
}