namespace Modules
{
	class Ads
	{
	public:
		Ads();

	private:
		static void CmdAddText_stub(int a1, int a2, const char* fmt, const char* dummy0, int dummy1, int dummy2, int dummy3, int dummy4, int dummy5);
		static void CmdAddText_hk(int a1, int a2, const char* fmt, const char* dummy0, int dummy1, int dummy2, int dummy3, int dummy4, int dummy5);
		static void __fastcall OpenTrack_stub(void* _this, DWORD edx, int a2, int a3, int a4, __int64 position, int a6, int a7);
		static void __fastcall OpenTrack_hk(void* _this, DWORD edx, int a2, int a3, int a4, __int64 position, int a6, int a7);
		static void __fastcall CreateTrack_stub(void* _this, DWORD edx, int a2, int a3, double speed, int a5, int a6, int flag, int a8, int a9);
		static void __fastcall CreateTrack_hk(void* _this, DWORD edx, int a2, int a3, double speed, int a5, int a6, int flag, int a8, int a9);
		static void __fastcall EnableSkips_hk(void* _this, DWORD edx);
		
	};
}