// ==========================================================
// Project: XSpotify
namespace Modules
{
	class Initialization
	{
	public:
		Initialization();

	private:
		static void XSpotifyWelcome();
		static void CheckFiles();
	};
}