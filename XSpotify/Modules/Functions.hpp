#pragma once

namespace Functions
{
	typedef void(__thiscall* CreateTrack_t)(void* _this, int a2, int a3, double speed, int a5, int a6, int flag, int a8, int a9);
	extern CreateTrack_t CreateTrack;

	typedef void(__thiscall* CloseTrack_t)(void* _this, int a2, int a3, int a4, int a5, int a6);
	extern CloseTrack_t CloseTrack;

	typedef  void(__thiscall* OpenTrack_t)(void* _this, int a2, int a3, int a4, int a5, __int64 position, int a7, int a8);
	extern OpenTrack_t OpenTrack;

	typedef void(__cdecl* CmdAddText_t)(int a1, int a2, const char* fmt, int dummy0, int dummy1, int dummy2, int dummy3, int dummy4, int dummy5);
	extern CmdAddText_t CmdAddText;

	typedef void(__cdecl* SetBitrate_t)(int a1, int a2, int a3, int a4, int a5);
	extern SetBitrate_t SetBitrate;

	typedef void(__thiscall* EnableSkips_t)(void* _this);
	extern EnableSkips_t EnableSkips;

	typedef void(__cdecl* GetFileID_t)(int* a1, int a2);
	extern GetFileID_t GetFileID;

	typedef void(__cdecl* aes_set_encrypt_key_t)(unsigned int* key, DWORD* userKey, int bits);
	extern aes_set_encrypt_key_t aes_set_encrypt_key;

	typedef void(__thiscall* Signal_t)(void* _this, int a2, int a3);
	extern Signal_t Signal;

	typedef void(__thiscall* SetLinkAvailableOffline_t)(void* __this, int a2, int* a3, unsigned int* a4, unsigned int a5, int* a6, unsigned __int8 a7);
	extern SetLinkAvailableOffline_t SetLinkAvailableOffline;

	typedef void(__thiscall* ParseUpdateUrl_t)(int a1, int a2);
	extern ParseUpdateUrl_t ParseUpdateUrl;
}